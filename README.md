# game-server-tools-ftc

## Installation
Python 3 needed

## Usage
```
python .\main.py campaign path-to-startup.cfg  // Activate mission logs of DServer
python .\main.py training path-to-startup.cfg  // De-Activate mission logs of DServer 
```

## License
WTFPL
