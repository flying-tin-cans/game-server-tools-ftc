import os
import sys

def main():
	print("Number of arguments:", len(sys.argv), "arguments.")
	print("Argument List:", str(sys.argv))

	if 3 == len(sys.argv):
		argument = sys.argv[1]
		if "campaign" == argument:
			search_str = "mission_text_log = 0"
			replace_str = "mission_text_log = 1"
			print("Activate mission logs")
		elif "training" == argument:
			search_str = "mission_text_log = 1"
			replace_str = "mission_text_log = 0"
			print("De-Activate mission logs")
		else:
			print("Allowed arguments in position 1:")
			print("- campaign")
			print("- training")
			sys.exit("Argument error")

		file_path = sys.argv[2]

		try:
			f = open(file_path, "r")
			old_content = f.read()
			f.close()
			new_content = old_content.replace(search_str, replace_str)
			f = open(file_path, "w")
			f.write(new_content)
			f.close()

		except FileNotFoundError as err:
			print(err)
			sys.exit("File error")
	else:
		print("Not the right numbers of arguments given")
		print("Usage is main.py [campaign/training] [path_to_config_file]")
		sys.exit("Argument error")

if __name__ == "__main__":
	main()
